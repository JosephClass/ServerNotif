package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "12345"
	dbname   = "postgres"
)

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {

	dbinfo := fmt.Sprintf("host = %s port = %d user = %s password = %s dbname = %s sslmode = disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", dbinfo)

	CheckError(err)

	defer db.Close()

	err1 := db.Ping()

	CheckError(err1)

	fmt.Println("Database Connected")

}
